const fs = require('fs');
const path = require('path');

// 指定文件夹路径
const folderPath = './';

// 读取文件夹内的所有文件
fs.readdir(folderPath, (err, files) => {
  if (err) {
    console.error('读取文件夹失败:', err);
    return;
  }

  // 遍历文件夹内的每个文件
  files.forEach((file) => {
    const filePath = path.join(folderPath, file);

    // 检查文件是否为图片
    if (isImageFile(file)) {
      // 提取文件名作为文件夹名
      const folderName = path.basename(file, path.extname(file));

      // 创建文件夹
      const newFolderPath = path.join(folderPath, folderName);
      fs.mkdir(newFolderPath, { recursive: true }, (err) => {
        if (err) {
          console.error('创建文件夹失败:', err);
          return;
        }

        // 移动图片到对应的文件夹
        const newFilePath = path.join(newFolderPath, file);
        fs.rename(filePath, newFilePath, (err) => {
          if (err) {
            console.error('移动图片失败:', err);
            return;
          }
          console.log(`图片 ${file} 已移动到文件夹 ${folderName}`);
        });
      });
    }
  });
});

// 检查文件是否为图片
function isImageFile(file) {
  const imageExtensions = ['.jpg', '.jpeg', '.png', '.gif'];
  const extension = path.extname(file).toLowerCase();
  return imageExtensions.includes(extension);
}